[![Licence GPL](http://img.shields.io/badge/license-GPL-green.svg)](http://www.gnu.org/licenses/quick-guide-gplv3.fr.html)
[![coverage report](https://gitlab.com/sia-insa-lyon/portailva-2/portailva-server/badges/master/coverage.svg)](https://gitlab.com/sia-insa-lyon/portailva-2/portailva-server/commits/master)
[![pipeline status](https://gitlab.com/sia-insa-lyon/portailva-2/portailva-server/badges/master/pipeline.svg)](https://gitlab.com/sia-insa-lyon/portailva-2/portailva-server/commits/master)

# PortailVA 2

PortailVA 2 is a web application that helps INSA Lyon's organizations
to properly manage their structure.

This application is the rewamped version of [PortailVA](https://gitlab.com/sia-insa-lyon/portailva). It can be forked to be tuned or improved.

## Troubleshooting

To report a bug or a dysfunction feel free to submit issues. 

## Deployment

This app can be deployed with `docker-compose`, just use `docker-compose up -d` :)

To run this app on your computer, you need to install dependencies using `docker exec portailva2-server composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts` and copy the environment file using `docker exec cp .env.local .env`.

Make sure that the ports `80`, `443` and `5432` are available on your computer. Help for deployment is provided on the wiki.

You will need the [front-end](https://gitlab.com/sia-insa-lyon/portailva-2/portailva-website) part of this application if you want to have as well the interface.

## Built With

- [Laravel](https://github.com/laravel/laravel) &mdash; Our back-end is a Laravel app using PHP 7.4.
- [PostgreSQL](http://www.postgresql.org/) &mdash; Our main data store is in Postgres.
- [Nginx](https://nginx.org/) &mdash; Our HTTP Server to serve our Laravel app.

And a lots of other dependencies you can find in [Composer](/composer.json) file.

## Contributions

After checking [GitLab Issues](https://gitlab.com/sia-insa-lyon/portailva-2/portailva-server/issues),
feel free to contribute by sending your pull requests.
While we may not merge your PR as is, they serve to start conversations 
and improve the general PortailVA experience for all users.

## Licence

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

```
PortailVA 2 - Managing platform for organizations
Copyright (C) 2019 SIA INSA Lyon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
