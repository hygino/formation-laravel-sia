# PHPStorm

See the following ressources if you want a more regular way to do it:
* An explanation on how to set [XDebug](https://lessthan12ms.com/docker-php-xdebug-phpstorm-good-developer-experience.html)
* Same but in French and more [info](https://blog.eleven-labs.com/fr/debug-run-phpunit-tests-using-docker-remote-interpreters-with-phpstorm/)

## Docker

When I do change on my DockerFile, I use the following command line to reset the container and the database volume
```
docker-compose down && sudo chmod -R 775 docker-db && docker-compose up -d --build
```

Now you have a working Laravel app running, we will need to configure PHPStorm.
Follow those steps:
* Go to File > Settings > Build, Execution, Deployment > Docker
* Add a new instance of Docker if there is none. On Ubuntu, the Docker daemon should be `Unix socket`
* Validate the new config
* Go to File > Settings > Languages & framework > PHP
* Click on the three dot on the CLI Interpreter line
* Add a new Interpreter and select `From Docker, ...`
* Select Docker in the new section and select `portailva2-server_app:latest`
* Validate the configuration and validate the window
* Select the new CLI and add the projet mapping
  * Click on the Folder icon for the "Docker container" input
  * In the `Volume bindings` section, add a mapping from your host path to the container path `/var/www` in order to be with the same configuration as defined by the docker-compose file

## XDebug

Now you have configured PHPStorm to use your Docker container, you will need to debug your PHP code. XDebug is a PHP Debugger that exactly allows you to do that.

Follow those steps:
* Create a new job by clinking on the MenuList item (previous to the play and debug buttons) and select `Edit Configurations...`
* Create a new Job `PHP remote debug` and give it a name
* Select the three dots button after the Server input
* The server should have the following content, if not set it as the following:
  * `app` as name
  * `localhost` as host
  * `80` as port
  * `XDebug` as debugger
  * `Use path mappings` checked
  * Your projet files folder mapped to `/var/www`
* Validate the server config
* Check the `Filter debug connection by IDE key` checkbox and define it as `PHPSTORM` in the `IDE key(session id)` input
* Validate the configuration
* Go to File > Settings > PHP > Debug
* Set the Xdebug port as `9001`

## PHPUnit

Now you have XDebug working, you will need to have PHPUnit working as well.
Follow those steps:
* Go to File > Settings > PHP > Test Framework
* Add a new framework `PHPUnit by Remote interpreter`
* Select the CLI interpreter associated to your Docker container (the one created previously) and validate.
* Click on the new Framework, select `Use Composer autoloader`, and set the `Path to script` input the path to the `./vendor/autoload.php` file
* Validate the configuration

You can now run test, for me, I have a global job to do that with the following configuration:
* Test scope: `Directory`
* Directory: `./tests`
* Preferred Coverage engine: `XDebug`
* Interpreter: `< Default project interpreter >`

If XDebug is properly set, you can also add a breakpoint in the PHP code. When running the test in debug mode, the IDE will stop at the proper breakpoint.

